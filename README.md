# 22_GD BMGroup

## How to launch:
#### Launch the server
1. Clone the repository: `git clone https://gitlab.com/Hofsiedge/22_gd-bmgroup.git .`
2. Open the server project in Visual Studio
3. Initialize database with `Update-Database` command in package manager console (this will apply expicit migrations and invoke `Seed` method, which fills DB with sample data)
4. Now you can start the server with IIS. When the defaut ASP.NET start page appears, there will be a link to API reference page in the navbar
5. You can create a user with a POST request on `/api/Account/Register`, for example, here is a Python script to do it:
```python
import requests

EMAIL       = 'email@mail.com'
PASSWORD    = 'Password_1'
PORT        = 44353

r = requests.post(
	f'https://localhost:{PORT}/api/Account/Register',
	verify=False,           # to turn the certificate verification off
	json={
	    "Email": EMAIL,
	    "Password": PASSWORD,
	    "ConfirmPassword": PASSWORD,
	})
print(r.status_code)
```
The same can be done with tools like Postman, curl, or any other. Here is a plain HTTP for the same request:
```http
POST /api/Account/Register HTTP/1.1
Host: localhost:44353
Content-Type: application/json

{
    "Email": "email@mail.com",
    "Password": "Password_1",
    "ConfirmPassword": "Password_1"
}
```
Note that the password must be at least 6 character long, contain both upper and lower case letters, digits and non-alphanumeric symbols (e.g. dots, dashes, underscores etc.)

#### Launch the admin client
1. Update the data in the config file `Client/Version_admin_1.0/Version_1.0/bin/Debug/config.txt` to match with the data you used to create a user. If the file is missing, create it according to the template:
```text
https://localhost:44353/
email@mail.com
Password_1
```
The first line is for the URL, the secong is for email, the third is for password

2. Build the admin client project
3. Run the .exe file or choose the `Debug` -> `Start without debugging` option in Visual Studio